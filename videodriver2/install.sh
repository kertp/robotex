# Get the Linux kernel source and a video utility: 

#cd /usr/src
#apt-get update
#apt-get install -y build-essential kernel-package linux-source guvcview
#tar --bzip2 -xvf linux-source-*.tar.bz2
#ln -s `find . -type d -name "linux-source-*"` linux

# Replace the driver source. This version of the driver has been patched to make videomode 0 (640x480 @ 15 fps) the default.

mv /usr/src/linux/drivers/media/video/gspca/ov534.c /usr/src/linux/drivers/media/video/gspca/ov534.c.original
cp ./ov534.c /usr/src/linux/drivers/media/video/gspca/

# Rebuild the driver modules ov534, gspca_ov534

cd /usr/src
cp -p linux-headers-$(uname -r)/Module.symvers linux

cd /usr/src/linux
make oldconfig
make modules_prepare
make SUBDIRS=drivers/media/video/gspca modules 

# Install the new gspca_ov534 driver

cd /lib/modules/$(uname -r)/kernel/drivers/media/video/gspca
mv gspca_ov534.ko gspca_ov534.ko.original
mv gspca_main.ko gspca_main.ko.original
cd /usr/src/linux/drivers/media/video/gspca
cp -p gspca_main.ko /lib/modules/$(uname -r)/kernel/drivers/media/video/gspca
cp -p gspca_ov534.ko /lib/modules/$(uname -r)/kernel/drivers/media/video/gspca

cd /lib/modules
depmod `uname -r`

# Kill Tekkotsu if it's running. Unplug the camera. Then to remove the old drivers and load the new ones, do:

modprobe -r gspca_ov534 gspca_main
modprobe gspca_ov534

