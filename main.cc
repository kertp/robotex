/*
03:33
Joonetuvastus tundub töötavat, alustame kohe vastse tuvastusega;

03:10
Kirjutame joonetuvastust

25.12, 02:11
Viimane töötav versioon Dropboxist. Osaline joonetuvastus olemas

24.11 20:19
Alustame joonetuvastusega

24.11 17:14
Alustame värava timeoutiga. VALMIS AGA TESTIMATA

VIIMATI MUUDETUD 24.11, 16:19
LIIKUMINE OK, SEDA NÄPPIDA EI TOHI!
1) joonetuvastus puudub
2) liikumine sitt - koha peal pööramine ära parandada
3) optokatkesti enam-vähem
4) mingi jama palli lugemisega

KASUTAB KONFIFAILI tty24_2

Kohapeal pööramisel viga sees.

Suurendasin % palli leidmisel

OPTIMISEERITUD

*/

/*NB! VASAKPOOLSE RATTA ID ON 4 JA PAREMPOOLSE RATTA ID ON 0!*/

#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <iostream>
#include <sstream>
#include <cstring>
#include <typeinfo>
#include <cmath>
#include <fstream>
#include <sys/time.h>
#include <sys/termios.h>    /* for ldterm - could of course also be used directly */
#include <sys/stropts.h>    /* for handling the STREAMS module */
#include <sys/ioctl.h>      /* ioctl() signature */
#include <sys/types.h>      /* for open() */
#include <sys/stat.h>       /* for open() */
#include <fcntl.h>          /* for open() */

#define _USE_MATH_DEFINES
#define BAUDRATE B115200
#define COIL "/dev/ttyACM1"
#define VASAKPOOLNE "/dev/ttyACM0"
#define PAREMPOOLNE "/dev/ttyACM2"
#define KOLLANE 3
#define SININE 2
#define PALL 0
#define VARAV 1

#include <getopt.h>
#include <cv.h>
#include <highgui.h>
#include <math.h>

#include "cmvision/capture.h"
#include "cmvision/vision.h"

using namespace std;
using namespace cv;


int leftS;
int rightS;
int loendur = 0;
int notFound = 0;
int parem;
int vasak;
int coil;
int poora;
int status = PALL;

// Joonte tuvastamiseks
int valjakupiir;
int valjakupiirX;
int valjakupiirX2;
int notShot;

int center_X = 0;
int center_Y = 0;

int nullike = 0;

// Keskpunktide tuvastamiseks
// Põhmõte: leiab, keskpuntki, joonistab selle ümber 10*10 ruudu ja kontrollib piksleid ruudu sees
int xKesk1;
int yKesk1;
int xKesk2;
int yKesk2;
int count_field;
const CMVision::region *checkreg;
int pallDribbleris;

bool varavaVarv; // Kui true, siis sinine

LowVision vision;
int loenkordi = 0;
int oigevastus = 0;

bool loe;
bool keera = false;



int avaSolenoid(const char* serial);
// int open_port(const char* serial);
int open_port(char *filename);
void incSpeed(int speed);
void setLeft(double speed);
void setRight(double speed);
void loo();
void turnRight(int deg);
void turnLeft(int deg);
void stop();
void liikumine(int koordinaat, int koordinaatY);
void rattaTuvastus(int Iport, int IIport, int vasakport, int paremport);
int getGoals(int serial);
int optokatkesti(int serial);
void taisnurkVasakule(double speed);
void taisnurkParemale(double speed);
void pooraVarav(int koordinaatX, int koordinaatY);
Capture initializeCapture ();
void palliEiLeitud(int x, int y);
int loeKiirus(int port);
void kirjutaSerialisse(int vasakport, int paremport);

int main(int argc,char **argv)
{
    vasak = open_port(VASAKPOOLNE);
    parem = open_port(PAREMPOOLNE);

    printf("Sisesta rünnatava värava värvi esimene täht (k/s): ");
    char varavaTaht = getchar();

    int varav = 0;
    if (varavaTaht == 'k')
    {
        printf("Ründame KOLLAST väravat");
        varav = KOLLANE;
        varavaVarv = false;
    }
    else if (varavaTaht == 's')
    {
        printf("Ründame SINIST väravat");
        varav = SININE;
        varavaVarv = true;
    }
    else 
    {
        printf("Panid vale tähe, pane programm uuesti käima!");
    }

    sleep(1);


    write(vasak, "dr0\n", strlen("dr0\n"));
    write(parem, "dr1\n", strlen("dr1\n"));

    write(vasak, "fs1\n", strlen("fs1\n"));
    write(parem, "fs1\n", strlen("fs1\n"));

    coil = avaSolenoid(COIL);

    bool ballfound = false;
    bool varavfound = false;
    bool eiOtsiPalli = false;

    IplImage* yuvimg=cvCreateImage(cvSize(640,480),IPL_DEPTH_8U,2);
    IplImage* rgbimg=cvCreateImage(cvSize(640,480),IPL_DEPTH_8U,3);

    int maxradius;
    float d_pikkus;
    float d_laius;
    int checktimes;
    
    Capture cap = initializeCapture();

    int mustPiksel = 0;
    int valgePiksel = 0;

    const CMVision::region *valjak;
    const CMVision::region *reg;
    const CMVision::region *sinineVarav;
    const CMVision::region *kollaneVarav;
    const CMVision::region *muu;
    // sleep(6);

    while(1)
    {
        varavfound = false;
        ballfound = false;
        if(loe)
        {
            loenkordi++;
            if ((optokatkesti(parem) == 1))
            {
                pallDribbleris++;
                status = varav;
                notFound = 5;
            }
            else
            {
                status = PALL;
            }
            loe = false;
        }
        else
        {
            loe = true;
        }


        if(notFound > 65)
        {
            eiOtsiPalli = true;
        }
        else
        {
            eiOtsiPalli = false;
        }
            
        const Capture::Image *img = cap.captureFrame();
        if(img != NULL)
        {
            cvLine(rgbimg, cvPoint(0, 300), cvPoint(640, 120), cvScalar(0, 255, 60, 0), 3, 8, 0);
            vision_image cmv_img;
            cmv_img.buf    = (pixel*)(img->data);
            cmv_img.width  = img->width;
            cmv_img.height = img->height;
            cmv_img.pitch  = img->bytesperline;
            cmv_img.field  = img->field;
            vision.processFrame(cmv_img);
            fflush(stdout);
            cap.releaseFrame(img);

            /*
            Väljaku tuvastamine. Joonistab kasti ümber sellele osale, mis ei ole väljak.
            Kasti ülemine koordinaat on  (x2,0) ja alumine koordinaat on (x1, y1)
            */
            yuvimg->imageData = (char *)(img->data);
            cvCvtColor(yuvimg, rgbimg, CV_YUV2BGR_YUYV);
            maxradius = 0;
            count_field = 0;
            /*
            *
            */
            muu = vision.getRegions(6);
            if(muu && (muu->area>200000))
            {
                incSpeed(-20);
                usleep(20000);
            }
            /*
            Pallide & joonte tuvastamine
            */
            if((status == PALL) && (eiOtsiPalli == false))
            {
                cout << "Otsin PALLI" << endl;
                reg = vision.getRegions(1);
                maxradius = 0;
                while(reg && reg->area>150)
                {
                    d_pikkus = reg->y2 - reg->y1;
                    d_laius = reg->x2 - reg->x1;

                    // Viimane tingimus testimata. Kui y2 liiga väike, siis pole kindlasti pall
                    // 12:34 kustutasin tingimus reg->y1 > 50
                    if ((reg->area > maxradius) && ((d_pikkus/d_laius > 0.7 && d_pikkus/d_laius < 1.8) || ((((reg->y2)+(reg->y1))*0.5) > 420)) && (reg->y1 > 50))
                    {
                        // cvRectangle(rgbimg, cvPoint(reg->x1, reg->y1), cvPoint(reg->x2, reg->y2), cvScalar(255, 0, 0, 0), 3, 8, 0);
                        // Kontrollib piksleid leitud palli ümbruses
                        if (d_pikkus < 20)
                        {
                            checktimes = 5;
                        }
                        else
                        {
                            checktimes = d_pikkus*0.1;
                        }
                        const CMVision::region *checkreg;
                        int cx, cy;
                        int dd = 10;
                        int count_field = 0;
                        int count_tot = 4*(checktimes+1);

                        for (int i = 0; i < checktimes; i++)
                        {
                            // x paigas & muudab y koordinaati
                            cx = reg->x1+(reg->x2-reg->x1)*i/checktimes;
                            cy = reg->y1-dd;

                            if (cx >= 640 || cx < 0 || cy >= 480 || cy < 0) count_tot--;
                            else {
                                checkreg = vision.findRegion(cx, cy);
                                if (checkreg && (checkreg->color == 1 || checkreg->color == 2 || checkreg->color == 3)) count_field +=1;
                                // cvCircle(rgbimg, cvPoint(cx, cy), 1, cvScalar(0, 0, 0), 1, 8, 0);
                            }

                            cy = reg->y2+dd;
                            if (cx >= 640 || cx < 0 || cy >= 480 || cy < 0) count_tot--;
                            else {
                                checkreg = vision.findRegion(cx, cy);
                                if (checkreg && (checkreg->color == 1 || checkreg->color == 2 || checkreg->color == 3)) count_field +=1;
                                // cvCircle(rgbimg, cvPoint(cx, cy), 1, cvScalar(0, 0, 0), 1, 8, 0);
                            }

                            // y paigas & muudab x koordinaati

                            cy = reg->y1+(reg->y2-reg->y1)*i/checktimes;

                            cx = reg->x1-dd;
                            if (cx >= 640 || cx < 0 || cy >= 480 || cy < 0) count_tot--;
                            else {
                                checkreg = vision.findRegion(cx, cy);
                                if (checkreg && (checkreg->color == 1 || checkreg->color == 2 || checkreg->color == 3)) count_field +=1;
                                // cvCircle(rgbimg, cvPoint(cx, cy), 1, cvScalar(0, 0, 0), 1, 8, 0);
                            }

                            cx = reg->x2+dd;
                            if (cx >= 640 || cx < 0 || cy >= 480 || cy < 0) count_tot--;
                            else {
                                checkreg = vision.findRegion(cx, cy);
                                if (checkreg && (checkreg->color == 1 || checkreg->color == 2 || checkreg->color == 3)) count_field +=1;
                                // cvCircle(rgbimg, cvPoint(cx, cy), 1, cvScalar(0, 0, 0), 1, 8, 0);
                            }
                        }

                        // Viimane tingimus testimata
                        if (count_tot > 0 && (100*count_field)/count_tot > 30){
                            mustPiksel = 0;
                            valgePiksel = 0;
                            cvRectangle(rgbimg, cvPoint(reg->x1, reg->y1), cvPoint(reg->x2, reg->y2), cvScalar(255,120, 100), 3, 8, 0);
                            cvLine(rgbimg, cvPoint(reg->x1, 480), cvPoint(reg->x1, reg->y2), cvScalar(0, 255, 60, 0), 3, 8, 0);
                            for(int i = reg->y2; i <= 480; i++)
                            {
                                checkreg = vision.findRegion(reg->x2, i);
                                if (checkreg && (checkreg->color == 6)) mustPiksel++;
                                else if (checkreg && (checkreg->color == 3)) valgePiksel++;
                                
                                checkreg = vision.findRegion(reg->x1, i);
                                if (checkreg && (checkreg->color == 6)) mustPiksel++;
                                else if (checkreg && (checkreg->color == 6)) valgePiksel++;
                            }
                            // cout << "Musti piksleid on " << mustPiksel;
                            // cout << "Valgeid piksleid on " << valgePiksel;
                            if((mustPiksel < 25) && (valgePiksel < 35))
                            {
                                center_X = ((reg->x2)+(reg->x1))*0.5;
                                center_Y = ((reg->y2)+(reg->y1))*0.5;
                                maxradius =  reg->area;
                                ballfound = true;
                                cvRectangle(rgbimg, cvPoint(reg->x1, reg->y1), cvPoint(reg->x2, reg->y2), cvScalar(0, 60, 255, 0), 3, 8, 0);
                            }
                            // 04:18, enne oli 10
                            else if((mustPiksel >= 25) && (valgePiksel >=35))
                            {
                                if(notFound < 5)
                                {
                                    notFound = 5;
                                }
                                else
                                {
                                    notFound++;
                                }
                                center_X = 0;
                                center_Y = 0;
                                taisnurkParemale(30);
                                usleep(20000);
                                incSpeed(20);
                                // taisnurkVasakule(30);
                                // usleep(30000);
                                // continue;
                            }
                        }
                    }
                    reg = reg->next;
                }
            }

            else if((status == KOLLANE) || ((notFound > 65) && (varavaVarv == false))){
                cout << "Otsin KOLLAST" << endl;
                kollaneVarav = vision.getRegions(4);
                int maxradius = 0;
                while(kollaneVarav && (kollaneVarav->area>2000))
                {
                    cvRectangle(rgbimg, cvPoint(kollaneVarav->x1, kollaneVarav->y1), cvPoint(kollaneVarav->x2, kollaneVarav->y2), cvScalar(0, 255, 60, 0), 3, 8, 0);
                    xKesk1 = (kollaneVarav->x1 + kollaneVarav->x2)*0.5 - 5;
                    yKesk1 = (kollaneVarav->y1 + kollaneVarav->y2)*0.5 - 5;
                    xKesk2 = (kollaneVarav->x1 + kollaneVarav->x2)*0.5 + 5;
                    yKesk2 = (kollaneVarav->y1 + kollaneVarav->y2)*0.5 + 5;
                    // cvRectangle(rgbimg, cvPoint(xKesk1, yKesk1), cvPoint(xKesk2 ,yKesk2), cvScalar(0, 255, 60, 0), 3, 8, 0);
                    count_field = 0;
                    for(int dx = 0; dx < 10; dx++){
                        checkreg = vision.findRegion(xKesk1 + dx, yKesk1 + dx);
                        if (checkreg && (checkreg->color == 4)) count_field++;
                    }
                    if(count_field > 4 && (kollaneVarav->area > maxradius)){
                        maxradius = kollaneVarav->area;
                        cvLine(rgbimg, cvPoint(kollaneVarav->x1, kollaneVarav->y1), cvPoint(kollaneVarav->x2, kollaneVarav->y2), cvScalar(0, 255, 60, 0), 3, 8, 0);
                        // cvLine(rgbimg, cvPoint(kollaneVarav->x2, kollaneVarav->y1), cvPoint(kollaneVarav->x1, kollaneVarav->y2), cvScalar(0, 255, 60, 0), 3, 8, 0);
                        center_X = xKesk1 + 5;
                        center_Y = yKesk1 + 5;
                        varavfound = true;
                        ballfound = false;
                    }
                    
                    kollaneVarav = kollaneVarav->next;
                }
            }
            else if((status == SININE)  || ((notFound > 65) && (varavaVarv == true))){
                cout << "Otsin SINIST" << endl;
                sinineVarav = vision.getRegions(5);
                int maxradius = 0;
                while(sinineVarav && sinineVarav->area>2000)
                {
                    xKesk1 = (sinineVarav->x1 + sinineVarav->x2)*0.5 - 5;
                    yKesk1 = (sinineVarav->y1 + sinineVarav->y2)*0.5 - 5;
                    xKesk2 = (sinineVarav->x1 + sinineVarav->x2)*0.5 + 5;
                    yKesk2 = (sinineVarav->y1 + sinineVarav->y2)*0.5 + 5;

                    
                    // cvRectangle(rgbimg, cvPoint(xKesk1, yKesk1), cvPoint(xKesk2 ,yKesk2), cvScalar(0, 255, 60, 0), 3, 8, 0);
                    count_field = 0;
                    for(int dx = 0; dx < 10; dx++){
                        checkreg = vision.findRegion(xKesk1 + dx, yKesk1 + dx);
                        if (checkreg && (checkreg->color == 5)) count_field++;
                    }
            
                    if(count_field > 8 && (sinineVarav->area > maxradius)){
                        cvLine(rgbimg, cvPoint(sinineVarav->x1, sinineVarav->y1), cvPoint(sinineVarav->x2, sinineVarav->y2), cvScalar(0, 255, 60, 0), 3, 8, 0);
                        maxradius = sinineVarav->area;
                        center_X = xKesk1 + 5;
                        center_Y = yKesk1 + 5;
                        varavfound = true;
                        ballfound = false;
                    }
                    sinineVarav = sinineVarav->next;
                }
            }

            cvShowImage("main", rgbimg);

            if (cvWaitKey(5) == 27)
            {
                break;
            }

            if (ballfound || (varavfound && (notFound > 80)))
            {
                if(ballfound)
                {
                    notFound = 0;
                    liikumine(center_X, center_Y);
                }
                // 23.11, 05:15, enne oli 40
                else if(center_Y > 30)
                {
                    notFound = 0;
                    varavaVarv = !(varavaVarv);
                    taisnurkParemale(30);
                }
                // Tähendab, et leidis värava, aga ei leia palli või otsis palli ja leidis ka selle
                else if(notFound > 130 && ((notFound % 130) == 0))
                {
                    varavaVarv = !(varavaVarv);
                    liikumine(center_X, center_Y);
                }
                
            }

            else if(varavfound)
            {   
                notFound = 0;
                pooraVarav(center_X, center_Y);
            }


            else
            {          
                /*
                Kui ei ole leidnud 5-30 kaadrit järjest, siis pööra
                */
                notFound++;
                if(pallDribbleris > 240)
                {
                    loo();
                }
                else if(notFound > 4)
                {
                    int deltaX = center_X - 320;
                    if(deltaX < 0)
                    {
                        // enne oli 0.07
                        taisnurkParemale(abs(deltaX * 0.08));
                    }
                    else if(deltaX > 0)
                    {
                        taisnurkVasakule(abs(deltaX * 0.08));
                    }
                }
                else
                {
                    liikumine(center_X, center_Y);
                }
                // loe = true;
            }

        }
    }
}

// SERIALI AVAMINE
int open_port(char *filename){
    struct termios tio;
    int tty;
    int ret;
    int i = 0;

    memset(&tio,0,sizeof(tio));
    tio.c_iflag=0;
    tio.c_oflag=0;
    tio.c_cflag=CS8|CREAD|CLOCAL;           // 8n1, see termios.h for more information
    tio.c_lflag=0;
    tio.c_cc[VMIN]=1;
    tio.c_cc[VTIME]=5;

    cfsetospeed(&tio,B115200);            // 115200 baud
    cfsetispeed(&tio,B115200);            // 115200 baud

    do {
        tty=open(filename, O_RDWR | O_NONBLOCK);
        // printf("VIGA: ei õnnestunud avada \"%s\" errno=%d\n", filename, errno);

        if( i == 100 ) exit(0);
        i++;
    }while( tty == -1  );


    ret = tcsetattr(tty,TCSANOW,&tio);
    if (ret == -1) {
        printf("VIGA: tcsetattr()=-1, errno=%d\n", errno);
        exit(1);
    }

    return tty;
}

// SOLENOIDKAHURI AVAMINE
int avaSolenoid(const char* serial){
    int fd; /* File descriptor for the port */
    fd = open(serial, O_RDWR | O_NOCTTY | O_NDELAY);
    // AVAMINE EBAÕNNESTUS
    if (fd == -1)
    {
        printf("Püssi avamine ebaõnnestus!\n");
    }
    else
    {
        struct termios oldtio,newtio;
        printf("Püssi avamine õnnestus!\n");
        fcntl(fd, F_SETFL, 0);
        tcgetattr(fd,&oldtio); /* save current serial port settings */
        bzero(&newtio, sizeof(newtio)); /* clear struct for new port settings */
        newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
        newtio.c_oflag = 0;
        tcflush(fd, TCIFLUSH);
        tcsetattr(fd,TCSANOW,&newtio);
        // Üritab nullida eelmistest kordadest jäänud pinget;
        int n = write(fd, "fs0\n", strlen("fs0\n"));
        if(n < 0)
            printf("Failsafe väljalülitamine ebaõnnestus!\n");
        usleep(40000);
        n = write(fd, "ac0\n", strlen("ac0\n"));
        if(n < 0)
            printf("Automaatse laadimise väljalülitamine ebaõnnestus!\n");
        printf("Võid nüüd voolu sisse lülitada\n");
        write(fd, "c\n", strlen("c\n"));
        getchar();
        return (fd);
    }
    return 0;
}

// Löömiseks
void loo(){
    stop();
    pallDribbleris = 0;
    loe = true;
    int n = write(coil, "k2300\n", strlen("k2300\n"));
    if(n < 0){
        printf("Solenoidkahuri tulistamine ebaõnnestus");
    }
    usleep(40000);
    write(coil, "c\n", strlen("c\n"));
    stop();
}


// SEAB VASAKU RATTA KIIRUSED
void setLeft(double speed){
    leftS = speed;
//// KONVERTIMINE
    ostringstream ostr;
    ostr << (int) leftS * (-1);
    string s = ostr.str();
    /*Kiirus on siin strini kujul, pean konvertima char jadaks.*/
    string kiirusS = string("sd") + s + "\n";
    char kiirus[kiirusS.size()+1];
    kiirusS.copy(kiirus, kiirusS.size());
    int n = write(vasak, kiirus, strlen(kiirus));
    if (n < 0)
      fputs("Vasaku ratta kiiruse muutmine ebaõnnestus!\n", stderr);
}
// SEAB PAREMA RATTA KIIRUSED
void setRight(double speed){
    rightS = speed;
    // KONVERTIMINE
    std::ostringstream ostr;
    ostr << (int) rightS *(-1);
    std::string s = ostr.str();
    /*Kiirus on siin stringi kujul, pean konvertima char jadaks.*/
    std::string kiirusS = std::string("sd") + s + "\n";
    char kiirus[kiirusS.size()+1];
    kiirusS.copy(kiirus, kiirusS.size());
    int n = write(parem, kiirus, strlen(kiirus));
    if (n < 0){
        fputs("Parema ratta kiiruse muutmine ebaõnnestus!\n", stderr);
    }
}

// TUVASTAB, KAS OTSIME VÄRAVAID VÕI PALLE
int optokatkesti(int serial)
{
    // puhvri uhtumiseks

    tcflush(serial, TCIOFLUSH);
    usleep(30000);
    char buf[50];
    write(serial, "ds\n", strlen("ds\n"));
    // write(serialII, "s\n", strlen("s\n"));
    usleep(30000);
    fcntl(serial, F_SETFL, FNDELAY);
    memset(buf,'0',sizeof(buf));
    int chkin=read(serial,buf,sizeof buf);
    if(chkin<0)
    {
        printf("Ei saa optokatkestilt signaali! Kontrolli parempoolset ratast!\n");
    }
    if((buf[0] == '0') || (buf[1] == '0')){
        oigevastus++;
        return PALL;
    }
    else if((buf[0] == '1') || (buf[1] == '1')){
        return VARAV;
    }
    return 0;
}

// OTSE LIIKUMINE
void incSpeed(int speed)
{
    if(keera)
    {
        stop();
        keera = false;
    }
    printf("Otse kiirusega %d\n", speed);
    setLeft(speed);
    setRight(speed);
}

// PAREMALE PÖÖRAMINE
void turnRight(int speed){
    if(keera)
    {
        stop();
        keera = false;
    }
    int vasakKiirus = 50;
    int paremKiirus = vasakKiirus - speed;
    setRight(paremKiirus);
    setLeft(vasakKiirus);
    cout << "Paremale kiirusega " << vasakKiirus << " ja " << paremKiirus << endl;
}

// VASAKULE PÖÖRAMINE
void turnLeft(int speed)
{
    if(keera)
    {
        stop();
        keera = false;
    }
    int paremKiirus = 50;
    int vasakKiirus = paremKiirus - speed;
    setLeft(vasakKiirus);
    setRight(paremKiirus);
    cout << "Vasakule kiirusega " << vasakKiirus << " ja " << paremKiirus << endl;
}

// PEATAMINE
void stop()
{
    setRight(0);
    setLeft(0);
    cout<<"Peatan"<<endl;
}

// 12:36
// enne oli incspeed(70)
// enne oli deltaX * 0.06
// Pööramised vahetatud - 12:50
void liikumine(int koordinaatX, int koordinaatY)
{
    int deltaX = koordinaatX - 320;
    // Kui lähedal, siis sõidab vaiksemalt
    if(koordinaatY > 350){
        incSpeed(30);
    }
    else if(deltaX < -60)
    {
        // 22:32, enne oli 0.07
        turnRight(abs(deltaX * 0.07));
    }
    else if(deltaX > 60)
    {
        turnLeft(deltaX * 0.07);
    }
    else
    {
        incSpeed(101);
    }
}

void pooraVarav(int koordinaatX, int koordinaatY){
    loe = true;
    int deltaX = koordinaatX - 320; 
    // printf("Löön %d\n", deltaX);
    if(deltaX < -60)
    {
         taisnurkParemale(abs(deltaX * 0.09));
    }
    else if(deltaX > 60)
    {
         taisnurkVasakule(abs(deltaX * 0.09));
    }
    else
    {
        loo();
    }
}
// Enne oli 7, nüüd 5
void taisnurkVasakule(double speed)
{
    keera = true;
    if(speed < 5)
    {
        speed = 5;
    }
    setRight(speed);
    setLeft(-speed);
    cout << "Täisnurk vasakule kiirusega " << speed << endl;
}

void taisnurkParemale(double speed)
{
    keera = true;
    if(speed < 5)
    {
        speed = 5;
    }
    setRight(-speed);
    setLeft(speed);
    cout << "Täisnurk paremale kiirusega " << speed << endl;
}

Capture initializeCapture () 
{
    Capture cap;

    const char *video_device = "/dev/video1";
    const int input_idx = 1;
    const int width  = 640;
    const int height = 480;

    cvNamedWindow("main", CV_WINDOW_AUTOSIZE);

    // initialize
    cap.init(video_device, input_idx, width, height, V4L2_PIX_FMT_YUYV);
    char tmap_file[64];

    snprintf(tmap_file, 64, "/home/pohhui/Dropbox/ROBO/confimiseks/tty24_kl330_M.tm");
    vision.init("/home/pohhui/Dropbox/ROBO/confimiseks/tty24_kl330_.col", tmap_file, width, height);

    return cap;
}
