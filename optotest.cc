/*
VIIMATI MUUDETUD 22.11, 03:42
1) Serialid OK;
2) Liikumine OK;
3) Kiirused sitad;
4) Timeoudid puuduvad;
5) Joonetuvastus;  

*/

/*NB! VASAKPOOLSE RATTA ID ON 6 JA PAREMPOOLSE RATTA ID ON 9!*/

#include <stdio.h>   /* Standard inupt/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <iostream>
#include <sstream>
#include <cstring>
#include <typeinfo>
#include <cmath>
#include <fstream>
#include <sys/time.h>
#include <sys/termios.h>    /* for ldterm - could of course also be used directly */
#include <sys/stropts.h>    /* for handling the STREAMS module */
#include <sys/ioctl.h>      /* ioctl() signature */
#include <sys/types.h>      /* for open() */
#include <sys/stat.h>       /* for open() */
#include <fcntl.h>          /* for open() */

#define _USE_MATH_DEFINES
#define BAUDRATE B115200
#define COIL "/dev/ttyACM0"
#define VASAKPOOLNE "/dev/ttyACM2"
#define PAREMPOOLNE "/dev/ttyACM1"
#define KOLLANE 3
#define SININE 2
#define PALL 0
#define VARAV 1

#include <getopt.h>
#include <cv.h>
#include <highgui.h>
#include <math.h>

#include "cmvision/capture.h"
#include "cmvision/vision.h"

using namespace std;
using namespace cv;


double leftS;
double rightS;
int loendur = 0;
int notFound = 0;
int parem;
int vasak;
int coil;
int poora;
int status = KOLLANE;

// Joonte tuvastamiseks
int valjakupiir;
int valjakupiirX;
int valjakupiirX2;
int notShot;

    int center_X = 0;
    
    int center_Y = 0;

// Keskpunktide tuvastamiseks
// Põhmõte: leiab, keskpuntki, joonistab selle ümber 10*10 ruudu ja kontrollib piksleid ruudu sees
int xKesk1;
int yKesk1;
int xKesk2;
int yKesk2;
int count_field;
const CMVision::region *checkreg;

bool varavaVarv; // Kui true, siis sinine
bool loeS; // kui true siis kiirus kui false siis optokaktesti

LowVision vision;
char reading[10];


// TUVASTAB, KAS OTSIME VÄRAVAID VÕI PALLE
int optokatkesti(int serial){
    char buf[50];
    write(serial, "ko\n", strlen("ko\n"));
    // write(serialII, "s\n", strlen("s\n"));
    usleep(10000);
    fcntl(serial, F_SETFL, FNDELAY);
    memset(buf,'0',sizeof(buf));
    int chkin=read(serial,buf,sizeof buf);
    if(chkin<0)
    {
        printf("Ei saa optokatkestilt signaali! Kontrolli parempoolset ratast!\n");
    }

    if((buf[3] == '0') || (buf[4] == '0')){
        return PALL;
    }
    else if((buf[3] == '1') || (buf[4] == '1')){
        return VARAV;
    }
}


// SERIALI AVAMINE
int open_port(const char* serial){
    int fd; /* File descriptor for the port */
    fd = open(serial, O_RDWR | O_NOCTTY | O_NDELAY);
    // AVAMINE EBAÕNNESTUS
    if (fd == -1){
        printf("Avamine ebaõnnestus!\n");
    }
    else{
        struct termios oldtio,newtio;
        printf("Avamine õnnestus!\n");
        fcntl(fd, F_SETFL, 0);
        tcgetattr(fd,&oldtio); /* save current serial port settings */
        bzero(&newtio, sizeof(newtio)); /* clear struct for new port settings */
        newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
        newtio.c_oflag = 0;
        tcflush(fd, TCIFLUSH);
        tcsetattr(fd,TCSANOW,&newtio);
        return (fd);
    }
}


int main(int argc,char **argv)
{
	parem = open_port(PAREMPOOLNE);
    sleep(1);

    while(1)
    {
    	if(optokatkesti(parem) == 0)
            {
                printf("PALL\n");
            }
            else
            {
                printf("VÄRAV\n");
                notFound = 0;
            }
        usleep(500000);
     }

}