CC=g++
CFLAGS = -c -Wall -g 
CFLAGS += -ffast-math `pkg-config --cflags opencv`
LDFLAGS = -lrt -lz `pkg-config --libs opencv`

SOURCES=$(wildcard *.cc) $(wildcard cmvision/*.cc) $(wildcard cmvision/util/*.cc)

OBJECTS=$(SOURCES:.cc=.o)
EXECUTABLE=test

CC_ECHO   = @echo Compiling: $@
LINK_ECHO = @echo Linking: $@
CLEAN_ECHO = @echo Cleaning.

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS)
	@$(CC) $(OBJECTS) -o $@ $(LDFLAGS)
	$(LINK_ECHO)

.cc.o:
	@$(CC) $(CFLAGS) $< -o $@
	$(CC_ECHO)

clean:
	@rm $(OBJECTS) $(EXECUTABLE)
	$(CLEAN_ECHO)
