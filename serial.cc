#include <stdio.h>   /* Standard input/output definitions */
#include <string>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <iostream>
#include <sstream>
#include <cstring>
#include <typeinfo>
#include <cmath>
#include <fstream>
#include <sys/time.h>
#include <sys/termios.h>    /* for ldterm - could of course also be used directly */
#include <sys/stropts.h>    /* for handling the STREAMS module */
#include <sys/ioctl.h>      /* ioctl() signature */
#include <sys/types.h>      /* for open() */
#include <sys/stat.h>       /* for open() */
#include <fcntl.h>          /* for open() */

using namespace std;

#define _USE_MATH_DEFINES
#define BAUDRATE B115200
#define VASAKPOOLNE "/dev/ttyACM0"
#define KOLLANE 3
#define SININE 2
#define PALL 1
#define VARAV 0

// SOLENOIDKAHURI AVAMINE
int avaSolenoid(const char* serial){
    int fd; /* File descriptor for the port */
    fd = open(serial, O_RDWR | O_NOCTTY | O_NDELAY);
    // AVAMINE EBAÕNNESTUS
    if (fd == -1)
    {
        printf("Püssi avamine ebaõnnestus!\n");
    }
    else
    {
        struct termios oldtio,newtio;
        printf("Püssi avamine õnnestus!\n");
        fcntl(fd, F_SETFL, 0);
        tcgetattr(fd,&oldtio); /* save current serial port settings */
        bzero(&newtio, sizeof(newtio)); /* clear struct for new port settings */
        newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
        newtio.c_oflag = 0;
        tcflush(fd, TCIFLUSH);
        tcsetattr(fd,TCSANOW,&newtio);
        // Üritab nullida eelmistest kordadest jäänud pinget;
        int n = write(fd, "fs0\n", strlen("fs0\n"));
        if(n < 0)
            printf("Failsafe väljalülitamine ebaõnnestus!\n");
        usleep(40000);
        n = write(fd, "ac0\n", strlen("ac0\n"));
        if(n < 0)
            printf("Automaatse laadimise väljalülitamine ebaõnnestus!\n");
        printf("Võid nüüd voolu sisse lülitada\n");
        write(fd, "c\n", strlen("c\n"));
        getchar();
        return (fd);
    }
}

// Löömiseks
void loo(int coil){
    int n;
    printf("TULISTA\n");
    n = write(coil, "k1500\n", strlen("k1500\n"));
    if(n < 0){
        printf("Solenoidkahuri tulistamine ebaõnnestus");
    }
    usleep(40000);
    write(coil, "c\n", strlen("c\n"));
}

int main()
{
	int vasak = avaSolenoid(VASAKPOOLNE);
	loo(vasak);
	sleep(2);
	printf("Olen löönud\n");
	write(vasak, "e\r", strlen("e\r"));
}
