#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <iostream>
#include <sstream>
#include <cstring>
#include <typeinfo>
#include <cmath>
#include <fstream>
#include <sys/time.h>
#include <sys/termios.h>    /* for ldterm - could of course also be used directly */
#include <sys/stropts.h>    /* for handling the STREAMS module */
#include <sys/ioctl.h>      /* ioctl() signature */
#include <sys/types.h>      /* for open() */
#include <sys/stat.h>       /* for open() */
#include <fcntl.h>          /* for open() */

#define _USE_MATH_DEFINES
#define BAUDRATE B115200

// SERIALI AVAMINE
int open_port(const char* serial){
    int fd; /* File descriptor for the port */
    fd = open(serial, O_RDWR | O_NOCTTY | O_NDELAY);
    // AVAMINE EBAÕNNESTUS
    if (fd == -1){
        printf("Avamine ebaõnnestus!\n");
    }
    else{
        struct termios oldtio,newtio;
        printf("Avamine õnnestus!\n");
        fcntl(fd, F_SETFL, 0);
        tcgetattr(fd,&oldtio); /* save current serial port settings */
        bzero(&newtio, sizeof(newtio)); /* clear struct for new port settings */
        newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
        newtio.c_oflag = 0;
        tcflush(fd, TCIFLUSH);
        tcsetattr(fd,TCSANOW,&newtio);
        return (fd);
    }
}

