/*
VIIMATI MUUDETUD 13.11
1) Kiirused suurendatud
2) Timeout puudu
*/

/*NB! VASAKPOOLSE RATTA ID ON 6 JA PAREMPOOLSE RATTA ID ON 9!*/

#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <iostream>
#include <sstream>
#include <cstring>
#include <typeinfo>
#include <cmath>
#include <fstream>
#include <sys/time.h>
#include <sys/termios.h>    /* for ldterm - could of course also be used directly */
#include <sys/stropts.h>    /* for handling the STREAMS module */
#include <sys/ioctl.h>      /* ioctl() signature */
#include <sys/types.h>      /* for open() */
#include <sys/stat.h>       /* for open() */
#include <fcntl.h>          /* for open() */

#define _USE_MATH_DEFINES
#define BAUDRATE B115200
#define COIL "/dev/ttyACM2"
#define VASAKPOOLNE "/dev/ttyACM1"
#define PAREMPOOLNE "/dev/ttyACM0"
#define KOLLANE 3
#define SININE 2
#define PALL 1
#define VARAV 0

#include <getopt.h>
#include <cv.h>
#include <highgui.h>
#include <math.h>

#include "cmvision/capture.h"
#include "cmvision/vision.h"

using namespace std;
using namespace cv;

double leftS;
double rightS;
int loendur = 0;
int notFound = 0;
int parem;
int vasak;
int coil;
int status = KOLLANE;

// Joonte tuvastamiseks
int valjakupiir;
int valjakupiirX;
int valjakupiirX2;

// Keskpunktide tuvastamiseks
// Põhmõte: leiab, keskpuntki, joonistab selle ümber 10*10 ruudu ja kontrollib piksleid ruudu sees
int xKesk1;
int yKesk1;
int xKesk2;
int yKesk2;
int count_field;
const CMVision::region *checkreg;


LowVision vision;
char reading[10];

int avaSolenoid(const char* serial);
int open_port(const char* serial);
void incSpeed(int speed);
void setLeft(double speed);
void setRight(double speed);
void loo();
void turnRight(int deg);
void turnLeft(int deg);
void stop();
void liikumine(int koordinaat, int koordinaatY);
void rattaTuvastus(int Iport, int IIport, int vasakport, int paremport);
int getGoals(int serial);
int optokatkesti(int serial);
void taisnurkVasakule(int speed);
void taisnurkParemale(int speed);
void pooraVarav(int koordinaatX, int koordinaatY);
Capture initializeCapture ();
void palliEiLeitud(int x, int y);

int main(int argc,char **argv)
{

    // Jadaliidese avamine, rataste ja solenoidi algseadistamine
    coil = avaSolenoid(COIL);
    vasak = open_port(VASAKPOOLNE);
    parem = open_port(PAREMPOOLNE);
    printf("Seadmed avatud\n");
    //  rattaTuvastus(portI, portII, vasak, parem);

    write(vasak, "dr0\n", strlen("dr0\n"));
    write(parem, "dr1\n", strlen("dr1\n"));
    write(vasak, "fs1\n", strlen("fs1\n"));
    write(parem, "fs1\n", strlen("fs1\n"));

    // Värava määramine

    int varav = getGoals(vasak);
    printf ("getGoal: %d\n", varav);
    
    while (1)
    {
        printf("Optokatkesti: %d\n", optokatkesti(parem));
        usleep(40000);
    }

    
}

// SERIALI AVAMINE
int open_port(const char* serial){
    int fd; /* File descriptor for the port */
    fd = open(serial, O_RDWR | O_NOCTTY | O_NDELAY);
    // AVAMINE EBAÕNNESTUS
    if (fd == -1){
        printf("Avamine ebaõnnestus!\n");
    }
    else{
        struct termios oldtio,newtio;
        printf("Avamine õnnestus!\n");
        fcntl(fd, F_SETFL, 0);
        tcgetattr(fd,&oldtio); /* save current serial port settings */
        bzero(&newtio, sizeof(newtio)); /* clear struct for new port settings */
        newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
        newtio.c_oflag = 0;
        tcflush(fd, TCIFLUSH);
        tcsetattr(fd,TCSANOW,&newtio);
        return (fd);
    }
}

// SOLENOIDKAHURI AVAMINE
int avaSolenoid(const char* serial){
    int fd; /* File descriptor for the port */
    fd = open(serial, O_RDWR | O_NOCTTY | O_NDELAY);
    // AVAMINE EBAÕNNESTUS
    if (fd == -1)
    {
        printf("Püssi avamine ebaõnnestus!\n");
    }
    else
    {
        struct termios oldtio,newtio;
        printf("Püssi avamine õnnestus!\n");
        fcntl(fd, F_SETFL, 0);
        tcgetattr(fd,&oldtio); /* save current serial port settings */
        bzero(&newtio, sizeof(newtio)); /* clear struct for new port settings */
        newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
        newtio.c_oflag = 0;
        tcflush(fd, TCIFLUSH);
        tcsetattr(fd,TCSANOW,&newtio);
        // Üritab nullida eelmistest kordadest jäänud pinget;
        int n = write(fd, "fs0\n", strlen("fs0\n"));
        if(n < 0)
            printf("Failsafe väljalülitamine ebaõnnestus!\n");
        usleep(40000);
        n = write(fd, "ac0\n", strlen("ac0\n"));
        if(n < 0)
            printf("Automaatse laadimise väljalülitamine ebaõnnestus!\n");
        printf("Võid nüüd voolu sisse lülitada\n");
        write(fd, "c\n", strlen("c\n"));
        getchar();
        return (fd);
    }
}

// Löömiseks
void loo(){
    stop();
    printf("TULISTA\n");
    int n = write(coil, "k1500\n", strlen("k1000\n"));
    if(n < 0){
        printf("Solenoidkahuri tulistamine ebaõnnestus");
    }
    usleep(40000);
    write(coil, "c\n", strlen("c\n"));
}


// SEAB VASAKU RATTA KIIRUSED
void setLeft(double speed){
    leftS = speed;
//// KONVERTIMINE
    ostringstream ostr;
    ostr << leftS;
    string s = ostr.str();
    /*Kiirus on siin strini kujul, pean konvertima char jadaks.*/
    string kiirusS = string("sd") + s + "\n";
    char kiirus[kiirusS.size()+1];
    kiirusS.copy(kiirus, kiirusS.size());
    int n = write(vasak, kiirus, strlen(kiirus));
    if (n < 0)
      fputs("Vasaku ratta kiiruse muutmine ebaõnnestus!\n", stderr);
}
// SEAB PAREMA RATTA KIIRUSED
void setRight(double speed){
    rightS = speed;
    // KONVERTIMINE
    std::ostringstream ostr;
    ostr << rightS * (-1);
    std::string s = ostr.str();
    /*Kiirus on siin stringi kujul, pean konvertima char jadaks.*/
    std::string kiirusS = std::string("sd") + s + "\n";
    char kiirus[kiirusS.size()+1];
    kiirusS.copy(kiirus, kiirusS.size());
    int n = write(parem, kiirus, strlen(kiirus));
    if (n < 0){
        fputs("Parema ratta kiiruse muutmine ebaõnnestus!\n", stderr);
    }
}

// TAGASTAB SELLE, MILLIST VÄRAVAT OTSIME

int getGoals(int serial){
        char sone[255];
        write(serial, "ko\n", strlen("ko\n"));
        usleep(40000);
        fcntl(serial, F_SETFL, FNDELAY);
        memset(sone,'0',sizeof(sone));
        int chkin=read(serial,sone,sizeof(sone));
        if(chkin<0)
        {
            printf("Värava tuvastamine ebaõnnestus!\n");
        }
        if((sone[3] == '1') || (sone[4] == '1')){
            cout << "Otsin SINIST väravat." << endl;
            return SININE;
        }
        else if((sone[3] == '0') || (sone[4] == '0')){
            cout << "Otsin KOLLAST väravat." << endl;
            return KOLLANE;
        }
}



// TUVASTAB, KAS OTSIME VÄRAVAID VÕI PALLE
int optokatkesti(int serial){
    char buf[50];
    write(serial, "ko\n", strlen("ko\n"));
//    usleep(40000);
    fcntl(serial, F_SETFL, FNDELAY);
    memset(buf,'0',sizeof(buf));
    int chkin=read(serial,buf,sizeof buf);
    if(chkin<0)
    {
        printf("Ei saa optokatkestilt signaali! Kontrolli parempoolset ratast!\n");
    }
    if((buf[3] == '1') || (buf[4] == '1')){
        cout << "VÄRAV" << endl;
        return VARAV;
    }
    else if((buf[3] == '0') || (buf[4] == '0')){
        cout << "PALL" << endl;
        return PALL;
    }
}


// RATASTE ID VÄLJASELGITAMINE
void rattaTuvastus(int Iport, int IIport, int vasakport, int paremport){
    // I RATTA PLAADILT ID LUGEMINE
    char buf[250];
    write(Iport, "?\n", strlen("?\n"));
    usleep(40000);
    fcntl(Iport, F_SETFL, FNDELAY);
    memset(buf,'0',sizeof(buf));
    int chkin=read(Iport,buf,sizeof(buf));
    if(chkin<0)
    {
        printf("Ei saa rattalt signaali! Kontrolli ühendust!\n");
//        exit(EXIT_FAILURE);
    }
    char esimene = buf[4];

    // II RATTA PLAADILT ID LUGEMINE
    write(IIport, "?\n", strlen("?\n"));
    usleep(40000);
    fcntl(IIport, F_SETFL, FNDELAY);
    memset(buf,'0',sizeof(buf));
    chkin=read(IIport,buf,sizeof(buf));
    if(chkin<0)
    {
        printf("Ei saa rattalt signaali! Kontrolli ühendust!\n");
//        exit(EXIT_FAILURE);
    }

    char teine = buf[4];

    cout << "Ühe plaadi ID on " << teine << " ja teise plaadi ID on " << esimene << endl;

    if((esimene == '6') && (teine == '9')){
        vasakport = esimene;
        paremport = teine;
//        printf("Avamine õnnestus!\n");
//        cout << "Vasakpoolse plaadi ID on " << esimene << " ja parempoolse plaadi ID on " << teine << ". OK!" << endl;
    }
    else if((esimene == '9') && (teine == '6')){
        vasakport = teine;
        paremport = esimene;
//        cout << "Vasakpoolse plaadi ID on " << teine << " ja parempoolse plaadi ID on " << esimene << ". OK!" << endl;
    }
    else{
//        cout << "Ühe plaadi ID on " << teine << " ja teise plaadi ID on " << esimene << ", aga kuskil ilmnes viga" << endl;
//        exit(EXIT_FAILURE);
    }
}


// OTSE LIIKUMINE
void incSpeed(int speed)
{
    printf("Otse kiirusega %d\n", speed);
    setLeft(speed);
    setRight(speed);
}

// PAREMALE PÖÖRAMINE
void turnRight(int speed){
    int vasakKiirus = 50;
    int paremKiirus = vasakKiirus - speed;
    setRight(paremKiirus);
    setLeft(vasakKiirus);
    cout << "Paremale kiirusega " << vasakKiirus << " ja " << paremKiirus << endl;
}

// VASAKULE PÖÖRAMINE
void turnLeft(int speed)
{
    int paremKiirus = 50;
    int vasakKiirus = paremKiirus - speed;
    setLeft(vasakKiirus);
    setRight(paremKiirus);
    cout << "Vasakule kiirusega " << vasakKiirus << " ja " << paremKiirus << endl;
}

// PEATAMINE
void stop()
{
    setRight(0);
    setLeft(0);
    cout<<"Peatan"<<endl;
}

void liikumine(int koordinaatX, int koordinaatY)
{
    int deltaX = koordinaatX - 320;
    if(koordinaatY > 200){
        deltaX = 0;
    }
    if(deltaX < -60)
    {
        turnRight(abs(deltaX * 0.08));
    }
    else if(deltaX > 60)
    {
        turnLeft(deltaX * 0.08);
    }
    else
    {
        incSpeed(50);
    }
}

void pooraVarav(int koordinaatX, int koordinaatY){
    int deltaX = koordinaatX - 320;
    // printf("Löön %d\n", deltaX);
    if(deltaX < -60)
    {
        taisnurkParemale(abs(deltaX * 0.06));
    }
    else if(deltaX > 60)
    {
        taisnurkVasakule(abs(deltaX * 0.06));
    }
    else
    {
        loo();
    }
}

void taisnurkVasakule(int speed)
{
    setRight(speed);
    setLeft(-speed);
}

void taisnurkParemale(int speed)
{
    setRight(-speed);
    setLeft(speed);
}

Capture initializeCapture () 
{
    Capture cap;

    const char *video_device = "/dev/video0";
    const int input_idx = 1;
    const int width  = 640;
    const int height = 480;

    cvNamedWindow("main", CV_WINDOW_AUTOSIZE);

    // initialize
    cap.init(video_device, input_idx, width, height, V4L2_PIX_FMT_YUYV);
    char tmap_file[64];

    snprintf(tmap_file, 64, "config/1830_9.tm");
    vision.init("config/1830_9.col", tmap_file, width, height);

    return cap;
}