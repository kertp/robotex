// hello.cc: A simple test program for CMVision2

// (C) 2005-2006 James R. Bruce, Carnegie Mellon University
// Licenced under the GNU General Public License (GPL) version 2,
//   or alternately by a specific written agreement.

#include <getopt.h>
#include <cv.h>
#include <highgui.h>
#include <math.h>

#include "cmvision/capture.h"
#include "cmvision/vision.h"

	LowVision vision;

Capture initializeCapture () 
{
	Capture cap;

	const char *video_device = "/dev/video10";
	const int input_idx = 1;
	const int width  = 640;
	const int height = 480;
	const int num_frames = 60;

	cvNamedWindow("main", CV_WINDOW_AUTOSIZE);

	// initialize
	cap.init(video_device, input_idx, width, height, V4L2_PIX_FMT_YUYV);
	char tmap_file[64];

	snprintf(tmap_file, 64, "config/ohtul.tm");
	vision.init("config/ohtul.col", tmap_file, width, height);

	return cap;
}


int main(int argc,char **argv)
{
	

	//FPSi kuvamiseks
	int fps = 0;
    struct timeval tempo1, tempo2;
    gettimeofday(&tempo1, NULL);

	IplImage* yuvimg=cvCreateImage(cvSize(640,480),IPL_DEPTH_8U,2);
	IplImage* rgbimg=cvCreateImage(cvSize(640,480),IPL_DEPTH_8U,3);

    
    int maxradius;
    float d_pikkus;
    float d_laius;
    int center_X;
    int center_Y;
    int checktimes;

    Capture cap = initializeCapture();

	while(true){
		const Capture::Image *img = cap.captureFrame();


		if(img != NULL)
		{
			vision_image cmv_img;
			cmv_img.buf    = (pixel*)(img->data);
			cmv_img.width  = img->width;
			cmv_img.height = img->height;
			cmv_img.pitch  = img->bytesperline;
			cmv_img.field  = img->field;
			vision.processFrame(cmv_img);
			printf(".");
			fflush(stdout);
			cap.releaseFrame(img);


        const CMVision::region *reg;
		reg = vision.getRegions(1);

        yuvimg->imageData = (char *)(img->data);
        cvCvtColor(yuvimg, rgbimg, CV_YUV2BGR_YUYV);
        maxradius = 0;
		while(reg && reg->area>15)
		{
		    d_pikkus = reg->y2 - reg->y1;
		    d_laius = reg->x2 - reg->x1;
		    center_X = ((reg->x2)+(reg->x1))*0.5;
		    center_Y = ((reg->y2)+(reg->y1))*0.5;

            cvRectangle(rgbimg, cvPoint(reg->x1, reg->y1), cvPoint(reg->x2, reg->y2), cvScalar(255, 0, 0, 0), 3, 8, 0);

		    if ( (reg->area > maxradius) && ((d_pikkus/d_laius > 0.7 && d_pikkus/d_laius < 1.4) || center_Y > 420))
		    {
		        if (d_pikkus < 20)
		        {
		            checktimes = 5;
		        }
		        else
		        {
		            checktimes = d_pikkus*0.1;
		        }

		        for (int i = 0; i < checktimes; i++)
		        {

		        }

                maxradius =  reg->area;
                cvRectangle(rgbimg, cvPoint(reg->x1, reg->y1), cvPoint(reg->x2, reg->y2), cvScalar(0, 255, 60, 0), 3, 8, 0);
                printf("X: %f, Y: %f Pikkus: %f\n", ((reg->x2)+(reg->x1))*0.5,  ((reg->y2)+(reg->y1))*0.5, d_pikkus);
		    }


            reg = reg->next;
		}

                fps++;
                gettimeofday(&tempo2, NULL);
                if (tempo2.tv_sec - tempo1.tv_sec ==1 ) {
                    printf("fps= %d\n",fps);
                    fps=0;
                    tempo1=tempo2;
                }

        cvShowImage("main", rgbimg);

            if (cvWaitKey(5) == 27)
            {
                break;
            }
		}


	}
	printf("\n");

	vision.close();
	cap.close();

	return(0);
}
