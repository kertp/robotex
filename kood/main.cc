/*
VIIMATI MUUDETUD 22.11, 04:56
Optokatkesti viga

timeoutil centery 40 asemel 50

timeout testimata, uus loogika

*/

/*NB! VASAKPOOLSE RATTA ID ON 4 JA PAREMPOOLSE RATTA ID ON 9!*/

#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <iostream>
#include <sstream>
#include <cstring>
#include <typeinfo>
#include <cmath>
#include <fstream>
#include <sys/time.h>
#include <sys/termios.h>    /* for ldterm - could of course also be used directly */
#include <sys/stropts.h>    /* for handling the STREAMS module */
#include <sys/ioctl.h>      /* ioctl() signature */
#include <sys/types.h>      /* for open() */
#include <sys/stat.h>       /* for open() */
#include <fcntl.h>          /* for open() */

#define _USE_MATH_DEFINES
#define BAUDRATE B115200
#define COIL "/dev/ttyACM1"
#define VASAKPOOLNE "/dev/ttyACM0"
#define PAREMPOOLNE "/dev/ttyACM2"
#define KOLLANE 3
#define SININE 2
#define PALL 0
#define VARAV 1

#include <getopt.h>
#include <cv.h>
#include <highgui.h>
#include <math.h>

#include "cmvision/capture.h"
#include "cmvision/vision.h"

using namespace std;
using namespace cv;


double leftS;
double rightS;
int loendur = 0;
int notFound = 0;
int parem;
int vasak;
int coil;
int poora;
int status = PALL;

// Joonte tuvastamiseks
int valjakupiir;
int valjakupiirX;
int valjakupiirX2;
int notShot;

    int center_X = 0;
    
    int center_Y = 0;

// Keskpunktide tuvastamiseks
// Põhmõte: leiab, keskpuntki, joonistab selle ümber 10*10 ruudu ja kontrollib piksleid ruudu sees
int xKesk1;
int yKesk1;
int xKesk2;
int yKesk2;
int count_field;
const CMVision::region *checkreg;

bool varavaVarv; // Kui true, siis sinine

LowVision vision;
char reading[10];

int avaSolenoid(const char* serial);
int open_port(const char* serial);
void incSpeed(int speed);
void setLeft(double speed);
void setRight(double speed);
void loo();
void turnRight(int deg);
void turnLeft(int deg);
void stop();
void liikumine(int koordinaat, int koordinaatY);
void rattaTuvastus(int Iport, int IIport, int vasakport, int paremport);
int getGoals(int serial);
int optokatkesti(int serial);
void taisnurkVasakule(double speed);
void taisnurkParemale(double speed);
void pooraVarav(int koordinaatX, int koordinaatY);
Capture initializeCapture ();
void palliEiLeitud(int x, int y);
int loeKiirus(int port);
void kirjutaSerialisse(int vasakport, int paremport);

int main(int argc,char **argv)
{
    vasak = open_port(VASAKPOOLNE);
    parem = open_port(PAREMPOOLNE);

    printf("Sisesta rünnatava värava värvi esimene täht (k/s): ");
    char varavaTaht = getchar();

    int varav = 0;
    if (varavaTaht == 'k')
    {
        printf("Ründame KOLLAST väravat");
        varav = KOLLANE;
        varavaVarv = false;
    }
    if (varavaTaht == 's')
    {
        printf("Ründame SINIST väravat");
        varav = SININE;
        varavaVarv = true;
    }

    sleep(1);

    write(vasak, "dr0\n", strlen("dr0\n"));
    write(parem, "dr1\n", strlen("dr1\n"));

    write(vasak, "fs1\n", strlen("fs1\n"));
    write(parem, "fs1\n", strlen("fs1\n"));

    coil = avaSolenoid(COIL);

    bool ballfound = false;
    bool varavfound = false;
    bool eiOtsiPalli = false;

    IplImage* yuvimg=cvCreateImage(cvSize(640,480),IPL_DEPTH_8U,2);
    IplImage* rgbimg=cvCreateImage(cvSize(640,480),IPL_DEPTH_8U,3);

    int maxradius;
    float d_pikkus;
    float d_laius;

    int checktimes;
    
    Capture cap = initializeCapture();

    const CMVision::region *valjak;
    const CMVision::region *reg;
    const CMVision::region *sinineVarav;
    const CMVision::region *kollaneVarav;

    while(1)
    {

        varavfound = false;
        ballfound = false;
        if(optokatkesti(parem) == 0)
        {
            printf("PALL\n");
            status = PALL;
        }
        else
        {
            printf("VÄRAV\n");
            status = varav;
            notFound = 5;
        }

        if(notFound > 120)
        {
            eiOtsiPalli = true;
        }
        else
        {
            eiOtsiPalli = false;
        }
            
        const Capture::Image *img = cap.captureFrame();
        if(img != NULL)
        {
            vision_image cmv_img;
            cmv_img.buf    = (pixel*)(img->data);
            cmv_img.width  = img->width;
            cmv_img.height = img->height;
            cmv_img.pitch  = img->bytesperline;
            cmv_img.field  = img->field;
            vision.processFrame(cmv_img);
            fflush(stdout);
            cap.releaseFrame(img);

            /*
            Väljaku tuvastamine. Joonistab kasti ümber sellele osale, mis ei ole väljak.
            Kasti ülemine koordinaat on  (x2,0) ja alumine koordinaat on (x1, y1)
            */
            yuvimg->imageData = (char *)(img->data);
            cvCvtColor(yuvimg, rgbimg, CV_YUV2BGR_YUYV);
            maxradius = 0;
            valjakupiir = 480;
            valjakupiirX = 0;
            valjakupiirX2 = 0;
            valjak = vision.getRegions(2);
            while(valjak && valjak->area>2000 && valjak->y1 > 100)
            {
                if(valjak->y1 < valjakupiir)
                {
                    xKesk1 = (valjak->x1 + valjak->x2)*0.5 - 5;
                    yKesk1 = (valjak->y1 + valjak->y2)*0.5 - 5;
                    count_field = 0;
                    for(int dx = 0; dx < 10; dx++)
                    {
                            checkreg = vision.findRegion(xKesk1 + dx, yKesk1 + dx);
                            if (checkreg && ((checkreg->color == 1) || (checkreg->color == 2) || (checkreg->color == 3)))
                            {
                                count_field++;
                            }
                    }
                    if(count_field > 7)
                    {
                        valjakupiir = valjak->y1;
                    }
                    
                }
                valjak = valjak->next;
            }
            cvRectangle(rgbimg, cvPoint(0, valjakupiir), cvPoint(640, 480), cvScalar(255, 120, 0, 0), 3, 8, 0);
            //printf("Väljaku piir on %d\n", valjakupiir);
            // if(valjakupiir > 400 && valjakupiir != 480)
            // {
            //     taisnurkParemale(30);
            //     continue;
            // }

            /*
            Pallide tuvastamine
            */
            if(status == PALL && eiOtsiPalli == false)
            {
                reg = vision.getRegions(1);
                maxradius = 0;
                while(reg && reg->area>50)
                {
                    d_pikkus = reg->y2 - reg->y1;
                    d_laius = reg->x2 - reg->x1;

                    // Viimane tingimus testimata. Kui y2 liiga väike, siis pole kindlasti pall
                    if (((reg->area > maxradius) && ((d_pikkus/d_laius > 0.7 && d_pikkus/d_laius < 1.8) || (((reg->y2)+(reg->y1))*0.5) > 420)) && (reg->y2 > 50))
                    {
                        cvRectangle(rgbimg, cvPoint(reg->x1, reg->y1), cvPoint(reg->x2, reg->y2), cvScalar(255, 0, 0, 0), 3, 8, 0);
                        // Kontrollib piksleid leitud palli ümbruses
                        if (d_pikkus < 20)
                        {
                            checktimes = 5;
                        }
                        else
                        {
                            checktimes = d_pikkus*0.1;
                        }
                        const CMVision::region *checkreg;
                        int cx, cy;
                        int dd = 6;
                        int count_field = 0;
                        int count_tot = 4*(checktimes+1);

                        for (int i = 0; i < checktimes; i++)
                        {
                            // x paigas & muudab y koordinaati
                            cx = reg->x1+(reg->x2-reg->x1)*i/checktimes;
                            cy = reg->y1-dd;

                            if (cx >= 640 || cx < 0 || cy >= 480 || cy < 0) count_tot--;
                            else {
                                checkreg = vision.findRegion(cx, cy);
                                if (checkreg && (checkreg->color == 1 || checkreg->color == 2 || checkreg->color == 3)) count_field +=1;
                                cvCircle(rgbimg, cvPoint(cx, cy), 1, cvScalar(0, 0, 0), 1, 8, 0);
                            }

                            cy = reg->y2+dd;
                            if (cx >= 640 || cx < 0 || cy >= 480 || cy < 0) count_tot--;
                            else {
                                checkreg = vision.findRegion(cx, cy);
                                if (checkreg && (checkreg->color == 1 || checkreg->color == 2 || checkreg->color == 3)) count_field +=1;
                                cvCircle(rgbimg, cvPoint(cx, cy), 1, cvScalar(0, 0, 0), 1, 8, 0);
                            }

                            // y paigas & muudab x koordinaati

                            cy = reg->y1+(reg->y2-reg->y1)*i/checktimes;

                            cx = reg->x1-dd;
                            if (cx >= 640 || cx < 0 || cy >= 480 || cy < 0) count_tot--;
                            else {
                                checkreg = vision.findRegion(cx, cy);
                                if (checkreg && (checkreg->color == 1 || checkreg->color == 2 || checkreg->color == 3)) count_field +=1;
                                cvCircle(rgbimg, cvPoint(cx, cy), 1, cvScalar(0, 0, 0), 1, 8, 0);
                            }

                            cx = reg->x2+dd;
                            if (cx >= 640 || cx < 0 || cy >= 480 || cy < 0) count_tot--;
                            else {
                                checkreg = vision.findRegion(cx, cy);
                                if (checkreg && (checkreg->color == 1 || checkreg->color == 2 || checkreg->color == 3)) count_field +=1;
                                cvCircle(rgbimg, cvPoint(cx, cy), 1, cvScalar(0, 0, 0), 1, 8, 0);
                            }
                        }

                        // Viimane tingimus testimata
                        if (count_tot > 0 && (100*count_field)/count_tot > 30){
                            center_X = ((reg->x2)+(reg->x1))*0.5;
                            center_Y = ((reg->y2)+(reg->y1))*0.5;
                            cvRectangle(rgbimg, cvPoint(reg->x1, reg->y1), cvPoint(reg->x2, reg->y2), cvScalar(0, 60, 255, 0), 3, 8, 0);
                            printf("X: %u, Y: %u Pikkus: %u\n", center_X,  center_Y, d_pikkus);
                            maxradius =  reg->area;
                            ballfound = true;
                            
                        }
                    }
                    reg = reg->next;
                }
            }

            else if(status == KOLLANE || (notFound > 120 && varavaVarv == false)){
                printf("OTSIN KOLLAST VÄRAVAT\n");
                kollaneVarav = vision.getRegions(4);
                int maxradius = 0;
                while(kollaneVarav && (kollaneVarav->area>2000))
                {
                    cvRectangle(rgbimg, cvPoint(kollaneVarav->x1, kollaneVarav->y1), cvPoint(kollaneVarav->x2, kollaneVarav->y2), cvScalar(0, 255, 60, 0), 3, 8, 0);
                    xKesk1 = (kollaneVarav->x1 + kollaneVarav->x2)*0.5 - 5;
                    yKesk1 = (kollaneVarav->y1 + kollaneVarav->y2)*0.5 - 5;
                    xKesk2 = (kollaneVarav->x1 + kollaneVarav->x2)*0.5 + 5;
                    yKesk2 = (kollaneVarav->y1 + kollaneVarav->y2)*0.5 + 5;
                    cvRectangle(rgbimg, cvPoint(xKesk1, yKesk1), cvPoint(xKesk2 ,yKesk2), cvScalar(0, 255, 60, 0), 3, 8, 0);
                    count_field = 0;
                    for(int dx = 0; dx < 10; dx++){
                        checkreg = vision.findRegion(xKesk1 + dx, yKesk1 + dx);
                        if (checkreg && (checkreg->color == 4)) count_field++;
                    }
                    if(count_field > 4 && (kollaneVarav->area > maxradius)){
                        maxradius = kollaneVarav->area;
                        cvLine(rgbimg, cvPoint(kollaneVarav->x1, kollaneVarav->y1), cvPoint(kollaneVarav->x2, kollaneVarav->y2), cvScalar(0, 255, 60, 0), 3, 8, 0);
                        cvLine(rgbimg, cvPoint(kollaneVarav->x2, kollaneVarav->y1), cvPoint(kollaneVarav->x1, kollaneVarav->y2), cvScalar(0, 255, 60, 0), 3, 8, 0);
                        center_X = xKesk1 + 5;
                        center_Y = yKesk1 + 5;
                        varavfound = true;
                        ballfound = false;
                    }
                    
                    kollaneVarav = kollaneVarav->next;
                }
            }
            else if(status == SININE  || (notFound > 120 && varavaVarv == true)){
                printf("OTSIN SINIST VÄRAVAT\n");
                sinineVarav = vision.getRegions(5);
                int maxradius = 0;
                while(sinineVarav && sinineVarav->area>2000)
                {
                    xKesk1 = (sinineVarav->x1 + sinineVarav->x2)*0.5 - 5;
                    yKesk1 = (sinineVarav->y1 + sinineVarav->y2)*0.5 - 5;
                    xKesk2 = (sinineVarav->x1 + sinineVarav->x2)*0.5 + 5;
                    yKesk2 = (sinineVarav->y1 + sinineVarav->y2)*0.5 + 5;

                    
                    // cvRectangle(rgbimg, cvPoint(xKesk1, yKesk1), cvPoint(xKesk2 ,yKesk2), cvScalar(0, 255, 60, 0), 3, 8, 0);
                    count_field = 0;
                    for(int dx = 0; dx < 10; dx++){
                        checkreg = vision.findRegion(xKesk1 + dx, yKesk1 + dx);
                        if (checkreg && (checkreg->color == 5)) count_field++;
                    }
                    printf("SININE %d\n", count_field);
            
                    if(count_field > 8 && (sinineVarav->area > maxradius)){
                        cvRectangle(rgbimg, cvPoint(sinineVarav->x1, sinineVarav->y1), cvPoint(sinineVarav->x2, sinineVarav->y2), cvScalar(0, 255, 60, 0), 3, 8, 0);
                        maxradius = sinineVarav->area;
                        center_X = xKesk1 + 5;
                        center_Y = yKesk1 + 5;
                        varavfound = true;
                        ballfound = false;
                    }
                    sinineVarav = sinineVarav->next;
                }
            }

            cvShowImage("main", rgbimg);

            if (cvWaitKey(5) == 27)
            {
                break;
            }

            if (ballfound || (varavfound && notFound > 120))
            {
                if(ballfound)
                {
                    notFound = 0;
                }
                else if(center_Y > 50)
                {
                    cout << "center_Y > 50" << endl;
                    notFound = 0;
                    cout << "Värava värv on" << varavaVarv << endl;
                    varavaVarv = !(varavaVarv);
                    cout << "Värava värv on" << varavaVarv << endl;
                }
                // Tähendab, et leidis värava, aga ei leia palli või otsis palli ja leidis ka selle
                else if(notFound > 240 && (notFound % 240 == 0))
                {
                    cout << "notFound > 240 && (notFound // 240 == 0)" << endl;
                    varavaVarv = !(varavaVarv);
                }
                liikumine(center_X, center_Y);
            }

            else if(varavfound)
            {   
                notFound = 0;
                pooraVarav(center_X, center_Y);
            }


            else
            {
                printf("Palli/väravat ei leitud! notFound: %d \n", notFound);              
                /*
                Kui ei ole leidnud 5-30 kaadrit järjest, siis pööra
                */
                notFound++;
                if(notFound > 4)
                {
                    int deltaX = center_X - 320;
                    if(deltaX < 0)
                    {
                        taisnurkVasakule(abs(deltaX * 0.06));
                    }
                    else if(deltaX > 0)
                    {
                        taisnurkParemale(abs(deltaX * 0.06));
                    }
                }
                else
                {
                    liikumine(center_X, center_Y);
                }

            }

    }
}
}

// SERIALI AVAMINE
int open_port(const char* serial){
    int fd; /* File descriptor for the port */
    fd = open(serial, O_RDWR | O_NOCTTY | O_NDELAY);
    // AVAMINE EBAÕNNESTUS
    if (fd == -1){
        printf("Avamine ebaõnnestus!\n");
    }
    else{
        struct termios oldtio,newtio;
        printf("Avamine õnnestus!\n");
        fcntl(fd, F_SETFL, 0);
        tcgetattr(fd,&oldtio); /* save current serial port settings */
        bzero(&newtio, sizeof(newtio)); /* clear struct for new port settings */
        newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
        newtio.c_oflag = 0;
        tcflush(fd, TCIFLUSH);
        tcsetattr(fd,TCSANOW,&newtio);
        return (fd);
    }
}

// SOLENOIDKAHURI AVAMINE
int avaSolenoid(const char* serial){
    int fd; /* File descriptor for the port */
    fd = open(serial, O_RDWR | O_NOCTTY | O_NDELAY);
    // AVAMINE EBAÕNNESTUS
    if (fd == -1)
    {
        printf("Püssi avamine ebaõnnestus!\n");
    }
    else
    {
        struct termios oldtio,newtio;
        printf("Püssi avamine õnnestus!\n");
        fcntl(fd, F_SETFL, 0);
        tcgetattr(fd,&oldtio); /* save current serial port settings */
        bzero(&newtio, sizeof(newtio)); /* clear struct for new port settings */
        newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
        newtio.c_oflag = 0;
        tcflush(fd, TCIFLUSH);
        tcsetattr(fd,TCSANOW,&newtio);
        // Üritab nullida eelmistest kordadest jäänud pinget;
        int n = write(fd, "fs0\n", strlen("fs0\n"));
        if(n < 0)
            printf("Failsafe väljalülitamine ebaõnnestus!\n");
        usleep(40000);
        n = write(fd, "ac0\n", strlen("ac0\n"));
        if(n < 0)
            printf("Automaatse laadimise väljalülitamine ebaõnnestus!\n");
        printf("Võid nüüd voolu sisse lülitada\n");
        write(fd, "c\n", strlen("c\n"));
        getchar();
        return (fd);
    }
}

// Löömiseks
void loo(){
    stop();
    printf("TULISTA\n");
    int n = write(coil, "k2000\n", strlen("k2000\n"));
    if(n < 0){
        printf("Solenoidkahuri tulistamine ebaõnnestus");
    }
    usleep(40000);
    write(coil, "c\n", strlen("c\n"));
}


// SEAB VASAKU RATTA KIIRUSED
void setLeft(double speed){
    leftS = speed;
//// KONVERTIMINE
    ostringstream ostr;
    ostr << leftS * (-1);
    string s = ostr.str();
    /*Kiirus on siin strini kujul, pean konvertima char jadaks.*/
    string kiirusS = string("sd") + s + "\n";
    char kiirus[kiirusS.size()+1];
    kiirusS.copy(kiirus, kiirusS.size());
    int n = write(vasak, kiirus, strlen(kiirus));
    if (n < 0)
      fputs("Vasaku ratta kiiruse muutmine ebaõnnestus!\n", stderr);
}
// SEAB PAREMA RATTA KIIRUSED
void setRight(double speed){
    rightS = speed;
    // KONVERTIMINE
    std::ostringstream ostr;
    ostr << rightS *(-1);
    std::string s = ostr.str();
    /*Kiirus on siin stringi kujul, pean konvertima char jadaks.*/
    std::string kiirusS = std::string("sd") + s + "\n";
    char kiirus[kiirusS.size()+1];
    kiirusS.copy(kiirus, kiirusS.size());
    int n = write(parem, kiirus, strlen(kiirus));
    if (n < 0){
        fputs("Parema ratta kiiruse muutmine ebaõnnestus!\n", stderr);
    }
}

// TAGASTAB SELLE, MILLIST VÄRAVAT OTSIME

int getGoals(int serial){
        char sone[255];
        write(serial, "ko\n", strlen("ko\n"));
        usleep(10000);
        fcntl(serial, F_SETFL, FNDELAY);
        memset(sone,'0',sizeof(sone));
        int chkin=read(serial,sone,sizeof(sone));
        if(chkin<0)
        {
            printf("Värava tuvastamine ebaõnnestus!\n");
        }
        if((sone[3] == '1') || (sone[4] == '1')){
            cout << "Otsin SINIST väravat." << endl;
            varavaVarv = true;
            return SININE;
        }
        else if((sone[3] == '0') || (sone[4] == '0')){
            cout << "Otsin KOLLAST väravat." << endl;
            varavaVarv = false;
            return KOLLANE;
        }
}

// TUVASTAB, KAS OTSIME VÄRAVAID VÕI PALLE
int optokatkesti(int serial){
    char buf[50];
    write(serial, "ko\n", strlen("ko\n"));
    // write(serialII, "s\n", strlen("s\n"));
    usleep(10000);
    fcntl(serial, F_SETFL, FNDELAY);
    memset(buf,'0',sizeof(buf));
    int chkin=read(serial,buf,sizeof buf);
    if(chkin<0)
    {
        printf("Ei saa optokatkestilt signaali! Kontrolli parempoolset ratast!\n");
    }

    if((buf[3] == '0') || (buf[4] == '0')){
        return PALL;
    }
    else if((buf[3] == '1') || (buf[4] == '1')){
        return VARAV;
    }
}


// RATASTE ID VÄLJASELGITAMINE
void rattaTuvastus(int Iport, int IIport, int vasakport, int paremport){
    // I RATTA PLAADILT ID LUGEMINE
    char buf[250];
    write(Iport, "?\n", strlen("?\n"));
    usleep(40000);
    fcntl(Iport, F_SETFL, FNDELAY);
    memset(buf,'0',sizeof(buf));
    int chkin=read(Iport,buf,sizeof(buf));
    if(chkin<0)
    {
        printf("Ei saa rattalt signaali! Kontrolli ühendust!\n");
//        exit(EXIT_FAILURE);
    }
    char esimene = buf[4];

    // II RATTA PLAADILT ID LUGEMINE
    write(IIport, "?\n", strlen("?\n"));
    usleep(40000);
    fcntl(IIport, F_SETFL, FNDELAY);
    memset(buf,'0',sizeof(buf));
    chkin=read(IIport,buf,sizeof(buf));
    if(chkin<0)
    {
        printf("Ei saa rattalt signaali! Kontrolli ühendust!\n");
//        exit(EXIT_FAILURE);
    }

    char teine = buf[4];

    cout << "Ühe plaadi ID on " << teine << " ja teise plaadi ID on " << esimene << endl;

    if((esimene == '6') && (teine == '9')){
        vasakport = esimene;
        paremport = teine;
//        printf("Avamine õnnestus!\n");
//        cout << "Vasakpoolse plaadi ID on " << esimene << " ja parempoolse plaadi ID on " << teine << ". OK!" << endl;
    }
    else if((esimene == '9') && (teine == '6')){
        vasakport = teine;
        paremport = esimene;
//        cout << "Vasakpoolse plaadi ID on " << teine << " ja parempoolse plaadi ID on " << esimene << ". OK!" << endl;
    }
    else{
//        cout << "Ühe plaadi ID on " << teine << " ja teise plaadi ID on " << esimene << ", aga kuskil ilmnes viga" << endl;
//        exit(EXIT_FAILURE);
    }
}


// OTSE LIIKUMINE
void incSpeed(int speed)
{
    printf("Otse kiirusega %d\n", speed);
    setLeft(speed);
    setRight(speed);
}

// PAREMALE PÖÖRAMINE
void turnRight(int speed){
    int vasakKiirus = 50;
    int paremKiirus = vasakKiirus - speed;
    setRight(paremKiirus);
    setLeft(vasakKiirus);
    cout << "Paremale kiirusega " << vasakKiirus << " ja " << paremKiirus << endl;
}

// VASAKULE PÖÖRAMINE
void turnLeft(int speed)
{
    int paremKiirus = 50;
    int vasakKiirus = paremKiirus - speed;
    setLeft(vasakKiirus);
    setRight(paremKiirus);
    cout << "Vasakule kiirusega " << vasakKiirus << " ja " << paremKiirus << endl;
}

// PEATAMINE
void stop()
{
    setRight(0);
    setLeft(0);
    cout<<"Peatan"<<endl;
}

void liikumine(int koordinaatX, int koordinaatY)
{
    int deltaX = koordinaatX - 320;
    if(koordinaatY > 400){
        deltaX = 0;
    }
    if(deltaX < -60)
    {
        turnLeft(abs(deltaX * 0.06));
    }
    else if(deltaX > 60)
    {
        turnRight(deltaX * 0.06);
    }
    else
    {
        incSpeed(70);
    }
}

void pooraVarav(int koordinaatX, int koordinaatY){
    int deltaX = koordinaatX - 320;
    // printf("Löön %d\n", deltaX);
    if(deltaX < -60)
    {
         taisnurkVasakule(abs(deltaX * 0.07));
    }
    else if(deltaX > 60)
    {
         taisnurkParemale(abs(deltaX * 0.07));
    }
    else
    {
        loo();
    }
}

void taisnurkVasakule(double speed)
{
    setRight(speed);
    setLeft(-speed);
}

void taisnurkParemale(double speed)
{
    setRight(-speed);
    setLeft(speed);
}

Capture initializeCapture () 
{
    Capture cap;

    const char *video_device = "/dev/video0";
    const int input_idx = 1;
    const int width  = 640;
    const int height = 480;

    cvNamedWindow("main", CV_WINDOW_AUTOSIZE);

    // initialize
    cap.init(video_device, input_idx, width, height, V4L2_PIX_FMT_YUYV);
    char tmap_file[64];

    snprintf(tmap_file, 64, "/home/pohhui/Dropbox/ROBO/confimiseks/1830_101.tm");
    vision.init("/home/pohhui/Dropbox/ROBO/confimiseks/1830_101.col", tmap_file, width, height);

    return cap;
}

// Takerdumise vältimiseks loeb kiirust
int loeKiirus(int serial)
{
    string str = "";
    char buf[50];
    write(serial, "s\n", strlen("s\n"));
    usleep(10000);
    fcntl(serial, F_SETFL, FNDELAY);
    memset(buf,'0',sizeof(buf));
    int chkin=read(serial,buf,sizeof buf);
    int loendaja = 3;
    while(buf[loendaja] != '>')
    {
        str += buf[loendaja];
        loendaja++;
    }
    cout << str << endl;
    int vasakuKiirus = atoi(str.c_str());
    return vasakuKiirus;
}
