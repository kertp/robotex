/*
29.10.2012 20:35
Väga algeline väljaku joonte tuvastus. Tuleb kõvasti kalibreerida.

28.10.2012, 16:09
* Olemas on:
    1) pallituvastus;
    2) kontrollib, kas on suurim ja kas on ruut;
    3) kontrollib ümbruses olevaid pikselid;
    4) lisatud juurde liikumine;

* Liikumist peab lihvima

* Pikslite kontrolli peab lihvima
*/

/*NB! VASAKPOOLSE RATTA ID ON 6 JA PAREMPOOLSE RATTA ID ON 9!*/

#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <iostream>
#include <sstream>
#include <cstring>
#include <typeinfo>
#include <cmath>
#include <fstream>
#include <sys/time.h>

#define _USE_MATH_DEFINES
#define BAUDRATE B115200
#define VASAKPOOLNE "/dev/ttyACM1"
#define PAREMPOOLNE "/dev/ttyACM0"
#define KOLLANE 3
#define SININE 2
#define PALL 1
#define VARAV 0

#include <getopt.h>
#include <cv.h>
#include <highgui.h>
#include <math.h>

#include "cmvision/capture.h"
#include "cmvision/vision.h"

using namespace std;
using namespace cv;

double leftS;
double rightS;
int loendur = 0;
int notFound = 0;
int parem;
int vasak;

int status = PALL;

int open_port(const char* serial);
void incSpeed(int speed);
void setLeft(double speed);
void setRight(double speed);
void turnRight(int deg);
void turnLeft(int deg);
void stop();
void liikumine(int koordinaat, int koordinaatY);
void rattaTuvastus(int Iport, int IIport, int vasakport, int paremport);
int getGoals(int serial);
int optokatkesti(int serial);
void taisnurkVasakule(int speed);
void taisnurkParemale(int speed);


int fpsLoendur = 0;
 // Kui praeguse ja eelmisekoordinaadi vahe < mingi konstant siis kiirust ei muuda
int xPrev;
// Palli leidmise boolean


char reading[10];

int main(int argc,char **argv)
{

    // Jadaliidese avamine ja rataste algseadistamine
    vasak = open_port(VASAKPOOLNE);
   parem = open_port(PAREMPOOLNE);

    //    rattaTuvastus(portI, portII, vasak, parem);

    write(vasak, "dr1\n", strlen("dr1\n"));
    write(parem, "dr0\n", strlen("dr0\n"));

    write(vasak, "fs1\n", strlen("fs1\n"));
    write(parem, "fs1\n", strlen("fs1\n"));

    // Värava määramine

    int varav = getGoals(vasak);
    bool ballfound = false;
	Capture cap;
	LowVision vision;

	//FPSi kuvamiseks
	int fps = 0;
    struct timeval tempo1, tempo2;
    gettimeofday(&tempo1, NULL);


	const char *video_device = "/dev/video0";
	const int input_idx = 1;
	const int width  = 640;
	const int height = 480;

    cvNamedWindow("main", CV_WINDOW_AUTOSIZE);

	IplImage* yuvimg=cvCreateImage(cvSize(640,480),IPL_DEPTH_8U,2);
	IplImage* rgbimg=cvCreateImage(cvSize(640,480),IPL_DEPTH_8U,3);

    // initialize
	cap.init(video_device, input_idx, width, height, V4L2_PIX_FMT_YUYV);
	char tmap_file[64];

	snprintf(tmap_file, 64, "config/1830_3.tm");
	vision.init("config/1830_3.col", tmap_file, width, height);

    int maxradius;
    float d_pikkus;
    float d_laius;
    int center_X = 0;
    int center_Y = 0;
    int checktimes;

    stop();     //Paneme ratastele 0-kiiruse

    int counter = 0;        //Dribbleri jaoks
    int antiCounter = 0;    //Dribbleri jaoks

	while(true)
	{
		const Capture::Image *img = cap.captureFrame();
		if(img != NULL)
		{
			vision_image cmv_img;
			cmv_img.buf    = (pixel*)(img->data);
			cmv_img.width  = img->width;
			cmv_img.height = img->height;
			cmv_img.pitch  = img->bytesperline;
			cmv_img.field  = img->field;
			vision.processFrame(cmv_img);
			fflush(stdout);
			cap.releaseFrame(img);

            const CMVision::region *reg;
            reg = vision.getRegions(2);

            yuvimg->imageData = (char *)(img->data);
            cvCvtColor(yuvimg, rgbimg, CV_YUV2BGR_YUYV);
            maxradius = 0;
            while(reg && reg->area>15)
            {
                xPrev = center_X;
                d_pikkus = reg->y2 - reg->y1;
                d_laius = reg->x2 - reg->x1;
                if(reg->area > maxradius){
//                     cvRectangle(rgbimg, cvPoint(reg->x1, reg->y1), cvPoint(reg->x2, reg->y2), cvScalar(255, 0, 0, 0), 3, 8, 0);
                    cvCircle(rgbimg, cvPoint(reg->x1, reg->y1), 10, cvScalar(255, 255, 0), 1, 8, 0);
                    cvCircle(rgbimg, cvPoint(reg->x2, reg->y2), 10, cvScalar(255, 255, 0), 1, 8, 0);
                    cvLine(rgbimg, cvPoint(reg->x1, reg->y1), cvPoint(reg->x2, reg->y1), cvScalar(255, 0, 0, 0), 1, 8, 0);
                     maxradius = reg->area;
                    printf("%d\n", reg->y1);
                }
                if(reg->y1 > 200){
                    taisnurkParemale(40);
//                    usleep(50000);
                }
                reg = reg->next;
            }


            fps++;
            gettimeofday(&tempo2, NULL);
            if (tempo2.tv_sec - tempo1.tv_sec ==1 ) {
                printf("fps= %d\n",fps);
                fps=0;
                tempo1=tempo2;
            }

            cvShowImage("main", rgbimg);

            if (cvWaitKey(5) == 27)
            {
                break;
            }
            if (ballfound)
            {
                ballfound = false;
                if(abs(center_X - xPrev) < 3 ){
                    center_X = xPrev;
                }
                liikumine(center_X, center_Y);
            }
            else
            {
                printf("Palli ei leitud!\n");
                /*
                Kui ei ole leidnud 3 kaadrit järjest, siis pööra
                */
                notFound++;
                if(notFound > 3)
                {
                // Kui palli ei leidnud, siis pöörab viimase koordinaadi järgi
                    int deltaX = center_X - 320;
                    if(deltaX > 30)
                    {
                        taisnurkParemale(6);
                    }
                    else if(deltaX < -30)
                    {
                        taisnurkVasakule(6);
                    }
                    else
                    {
                        taisnurkParemale(6);
                    }
                }
                else
                {
                    liikumine(center_X, center_Y);
                }
            }
        }
	printf("\n");
    }
    vision.close();
	cap.close();

	return(0);
}

// SERIALI AVAMINE
int open_port(const char* serial){
    int fd; /* File descriptor for the port */
    fd = open(serial, O_RDWR | O_NOCTTY | O_NDELAY);
    // AVAMINE EBAÕNNESTUS
    if (fd == -1){
        printf("Avamine ebaõnnestus!\n");
    }
    else{
        struct termios oldtio,newtio;
        printf("Avamine õnnestus!\n");
        fcntl(fd, F_SETFL, 0);
        tcgetattr(fd,&oldtio); /* save current serial port settings */
        bzero(&newtio, sizeof(newtio)); /* clear struct for new port settings */
        newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
        newtio.c_oflag = 0;
        tcflush(fd, TCIFLUSH);
        tcsetattr(fd,TCSANOW,&newtio);
        return (fd);
    }
}

// SEAB VASAKU RATTA KIIRUSED
void setLeft(double speed){
    leftS = speed;
//// KONVERTIMINE
    ostringstream ostr;
    ostr << leftS * (-1);
    string s = ostr.str();
    /*Kiirus on siin strini kujul, pean konvertima char jadaks.*/
    string kiirusS = string("sd") + s + "\n";
    char kiirus[kiirusS.size()+1];
    kiirusS.copy(kiirus, kiirusS.size());
    int n = write(vasak, kiirus, strlen(kiirus));
    if (n < 0)
      fputs("Vasaku ratta kiiruse muutmine ebaõnnestus!\n", stderr);
}
// SEAB PAREMA RATTA KIIRUSED
void setRight(double speed){
    rightS = speed;
    // KONVERTIMINE
    std::ostringstream ostr;
    ostr << rightS;
    std::string s = ostr.str();
    /*Kiirus on siin stringi kujul, pean konvertima char jadaks.*/
    std::string kiirusS = std::string("sd") + s + "\n";
    char kiirus[kiirusS.size()+1];
    kiirusS.copy(kiirus, kiirusS.size());
    int n = write(parem, kiirus, strlen(kiirus));
    if (n < 0){
        fputs("Parema ratta kiiruse muutmine ebaõnnestus!\n", stderr);
    }
}

// TAGASTAB SELLE, MILLIST VÄRAVAT OTSIME
int getGoals(int serial){
        char sone[255];
        write(serial, "ko\n", strlen("ko\n"));
//        usleep(40000);
        fcntl(serial, F_SETFL, FNDELAY);
        memset(sone,'0',sizeof(sone));
        int chkin=read(serial,sone,sizeof(sone));
        if(chkin<0)
        {
            printf("Värava tuvastamine ebaõnnestus!\n");
        }
        cout << sone << endl;
        if((sone[3] == '1') || (sone[4] == '1')){
            cout << "Otsin SINIST väravat." << endl;
            return SININE;
        }
        else if((sone[3] == '0') || (sone[4] == '0')){
            cout << "Otsin KOLLAST väravat." << endl;
            return KOLLANE;
        }
}

// TUVASTAB, KAS OTSIME VÄRAVAID VÕI PALLE
int optokatkesti(int serial){
    char buf[50];
    write(serial, "ko\n", strlen("ko\n"));
//    usleep(40000);
    fcntl(serial, F_SETFL, FNDELAY);
    memset(buf,'0',sizeof(buf));
    int chkin=read(serial,buf,sizeof buf);
    if(chkin<0)
    {
        printf("Ei saa optokatkestilt signaali! Kontrolli parempoolset ratast!\n");
    }
    if((buf[3] == '1') || (buf[4] == '1')){
        cout << "VÄRAV" << endl;
        return VARAV;
    }
    else if((buf[3] == '0') || (buf[4] == '0')){
        cout << "PALL" << endl;
        return PALL;
    }
}


// RATASTE ID VÄLJASELGITAMINE
void rattaTuvastus(int Iport, int IIport, int vasakport, int paremport){
    // I RATTA PLAADILT ID LUGEMINE
    char buf[250];
    write(Iport, "?\n", strlen("?\n"));
    usleep(40000);
    fcntl(Iport, F_SETFL, FNDELAY);
    memset(buf,'0',sizeof(buf));
    int chkin=read(Iport,buf,sizeof(buf));
    if(chkin<0)
    {
        printf("Ei saa rattalt signaali! Kontrolli ühendust!\n");
//        exit(EXIT_FAILURE);
    }
    char esimene = buf[4];

    // II RATTA PLAADILT ID LUGEMINE
    write(IIport, "?\n", strlen("?\n"));
    usleep(40000);
    fcntl(IIport, F_SETFL, FNDELAY);
    memset(buf,'0',sizeof(buf));
    chkin=read(IIport,buf,sizeof(buf));
    if(chkin<0)
    {
        printf("Ei saa rattalt signaali! Kontrolli ühendust!\n");
//        exit(EXIT_FAILURE);
    }

    char teine = buf[4];

    cout << "Ühe plaadi ID on " << teine << " ja teise plaadi ID on " << esimene << endl;

    if((esimene == '6') && (teine == '9')){
        vasakport = esimene;
        paremport = teine;
//        printf("Avamine õnnestus!\n");
//        cout << "Vasakpoolse plaadi ID on " << esimene << " ja parempoolse plaadi ID on " << teine << ". OK!" << endl;
    }
    else if((esimene == '9') && (teine == '6')){
        vasakport = teine;
        paremport = esimene;
//        cout << "Vasakpoolse plaadi ID on " << teine << " ja parempoolse plaadi ID on " << esimene << ". OK!" << endl;
    }
    else{
//        cout << "Ühe plaadi ID on " << teine << " ja teise plaadi ID on " << esimene << ", aga kuskil ilmnes viga" << endl;
//        exit(EXIT_FAILURE);
    }
}


// OTSE LIIKUMINE
void incSpeed(int speed)
{
    printf("Otse kiirusega %d\n", speed);
    setLeft(speed);
    setRight(speed);
}

// PAREMALE PÖÖRAMINE
void turnRight(int speed){
    int vasakKiirus = 0;
    int paremKiirus = 0;
    if(leftS == 0){
        leftS = speed;
    }
    vasakKiirus = abs(leftS);
    paremKiirus = vasakKiirus - speed;
    setRight(paremKiirus);
    setLeft(vasakKiirus);
    cout << "Paremale kiirusega " << vasakKiirus << " ja " << paremKiirus << endl;
}

// VASAKULE PÖÖRAMINE
void turnLeft(int speed)
{
    int vasakKiirus = 0;
    int paremKiirus = 0;
    if(rightS == 0){
        rightS = speed;
    }
    paremKiirus = abs(rightS);
    vasakKiirus = paremKiirus - speed;
    setLeft(vasakKiirus);
    setRight(paremKiirus);
    cout << "Vasakule kiirusega " << vasakKiirus << " ja " << paremKiirus << endl;
}

// PEATAMINE
void stop()
{
    setRight(0);
    setLeft(0);
    cout<<"Peatan"<<endl;
}

void liikumine(int koordinaatX, int koordinaatY)
{
    int deltaX = koordinaatX - 320;
    if(koordinaatY > 110){
        deltaX = 0;
    }
    if(deltaX > 60)
    {
        turnRight(deltaX * 0.03);
    }
    else if(deltaX < -60)
    {
        turnLeft(abs(deltaX * 0.03));
    }
    else
    {
        incSpeed(30);
    }
}

void taisnurkVasakule(int speed)
{
    setRight(speed);
    setLeft(-speed);
}

void taisnurkParemale(int speed)
{
    setRight(-speed);
    setLeft(speed);
}
