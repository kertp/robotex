// hello.cc: A simple test program for CMVision2

// (C) 2005-2006 James R. Bruce, Carnegie Mellon University
// Licenced under the GNU General Public License (GPL) version 2,
//   or alternately by a specific written agreement.

#include <getopt.h>
#include <cv.h>
#include <highgui.h>
#include <math.h>

#include "cmvision/capture.h"
#include "cmvision/vision.h"

int main(int argc,char **argv)
{
	Capture cap;
	LowVision vision;

	const char *video_device = "/dev/video10";
	const int input_idx = 1;
	const int width  = 640;
	const int height = 480;
	const int num_frames = 60;

    cvNamedWindow("main", CV_WINDOW_AUTOSIZE);

	IplImage* yuvimg=cvCreateImage(cvSize(640,480),IPL_DEPTH_8U,2);
	IplImage* rgbimg=cvCreateImage(cvSize(640,480),IPL_DEPTH_8U,3);

    // initialize
	cap.init(video_device, input_idx, width, height, V4L2_PIX_FMT_YUYV);
	char tmap_file[64];

	snprintf(tmap_file, 64, "config/1830_7.tm");
	vision.init("config/1830_7.col", tmap_file, width, height);
    int valjakupiir;
    int valjakupiirX;
    int valjakuPiirX2;

	while(true){
		const Capture::Image *img = cap.captureFrame();


		if(img != NULL)
		{
			vision_image cmv_img;
			cmv_img.buf    = (pixel*)(img->data);
			cmv_img.width  = img->width;
			cmv_img.height = img->height;
			cmv_img.pitch  = img->bytesperline;
			cmv_img.field  = img->field;
			vision.processFrame(cmv_img);
			fflush(stdout);
			cap.releaseFrame(img);


                const CMVision::region *reg;
    		reg = vision.getRegions(2);

            yuvimg->imageData = (char *)(img->data);
            cvCvtColor(yuvimg, rgbimg, CV_YUV2BGR_YUYV);
            valjakupiir = 480;


		  while(reg && reg->area>10000)
		  {
               printf("%d\n", reg->y1);
                if(reg->y1 < valjakupiir)
                {
                    valjakupiir = reg->y1;
                    valjakupiirX = reg -> x1;
                    valjakuPiirX2 = reg -> x2;

                }

                reg = reg->next;
		  }
           cvRectangle(rgbimg, cvPoint(valjakupiirX, valjakupiir), cvPoint(valjakuPiirX2, 0), cvScalar(255, 0, 0, 0), 3, 8, 0);

        cvShowImage("main", rgbimg);

            if (cvWaitKey(5) == 27)
            {
                break;
            }
		}


	}

	vision.close();
	cap.close();

	return(0);
}
